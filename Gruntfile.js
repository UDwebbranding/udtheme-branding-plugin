//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

	// All upfront config goes in a massive nested object.
	grunt.initConfig({
		// You can set arbitrary key-value pairs.
		distFolder: 'dist',
		copy: {
			build: {
				cwd: 'src',
				src: [ '**' ],
				dest: '<%= distFolder %>',
				expand: true
			},
		},
		clean: {
			build: {
				src: [ '<%= distFolder %>' ]
			},
			stylesheets: {
					src: [ '<%= distFolder %>/**/*.css' ]
				},
				scripts: {
					src: [ '<%= distFolder %>/**/*.js' ]
				},
		},

		uglify: {
			build: {
				files: {
					'<%= distFolder %>/main.min.js': ['dist/**/*.js']
				}
			}
		},

		cssmin: {
			build: {
				files: {
					'<%= distFolder %>/main.min.css': [ 'dist/**/*.css' ]
				}
			}
		},
		watch: {
			stylesheets: {
				files: 'src/**/*.css',
				tasks: [ 'stylesheets' ]
			},
			scripts: {
				files: 'src/**/*.js',
				tasks: [ 'scripts' ]
			},
			copy: {
				files: [ 'src/**' ],
				tasks: [ 'copy' ]
			}
		},






		// You can also set the value of a key as parsed JSON.
		// Allows us to reference properties we declared in package.json.
		pkg: grunt.file.readJSON('package.json'),
		// Grunt tasks are associated with specific properties.
		// these names generally match their npm package name.

	}); // The end of grunt.initConfig

	// We've set up each task's configuration.
	// Now actually load the tasks.
	// This will do a lookup similar to node's require() function.

	//grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Register our own custom task alias.
	grunt.registerTask(
		'stylesheets',
		'Compiles the stylesheets.',
		[ 'cssmin', 'clean:stylesheets' ]
	);

	grunt.registerTask(
		'scripts',
		'Compiles the JavaScript files.',
		[ 'uglify', 'clean:scripts' ]
	);

	grunt.registerTask(
		'build',
		'Compiles all of the assets and copies the files to the build directory.',
		[ 'clean:build', 'copy', 'stylesheets', 'scripts' ]
	);
};
