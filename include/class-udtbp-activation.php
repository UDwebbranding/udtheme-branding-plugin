<?php
/**
 * Class: UDTheme Branding Activation
 *
 * The purpose of this class is to:
 * Register activation hook
 * Fire activation hook
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @package     udtheme-brand
 * @subpackage  udtheme-brand/include
 * @author      Christopher Leonard
 * @license     GPLv3 or Later
 * @copyright   Copyright (c)2012-2017 Christopher Leonard University of Delaware
 * @version     3.1.0
 */
if ( ! class_exists( 'udtbp_Activation' ) ) :
  class udtbp_Activation {
  /**
   * The ID of this plugin.
   *
   * @since    2.3.1
   * @version  1.0.0                           New name introduced.
   * @access   private
   * @var      string         $udtbp           The ID of this plugin.
  */
   private $udtbp;
    /**
     * CLASS INITIALIZATION
     * Initiates the class and set its properties.
     *
     * @since     3.0.0
     * @param     string    $udtbp       The name of this plugin.
     */
    public function __construct( $udtbp ) {
      $this->udtbp = $udtbp;
    }
  	/**
     * REGISTER ACTIVATION HOOK
     *
     * Register hooks that are fired when the plugin is activated.
     * When the plugin is deleted, the uninstall.php file is loaded.
     *
     * @since 3.0.0
     */
    public function udtbp_start_activation() {
      $this->udtbp_requirements_check(); //php and wp version check method
    }
      /**
       * REQUIREMENTS CHECK
       *
       * Make sure current configuration meets minimum requirements
       * to install plugin.
       *
       * @since 3.0.0
       * @version 1.5.0 Sanitize html for sanity
       */
    public function udtbp_requirements_check() {
      global $wp_version;
      if ( version_compare( PHP_VERSION, UDTBP_REQ_PHP_VERSION, '<' ) ) {
      ?>
      <ul class="ul-disc">
        <li>
          <strong>PHP <?php echo esc_html_e( UDTBP_REQ_PHP_VERSION ); ?> version is the minimum requirement to install plugin.</strong>
          <em>( You're running version <?php echo esc_html_e( PHP_VERSION ); ?> )</em>
        </li>
      </ul>
    <?php
      return false;
      }

      if ( version_compare( $wp_version, UDTBP_REQ_WP_VERSION, '<' ) ) {
        ?>
      <ul class="ul-disc">
        <li>
          <strong>WordPress <?php echo esc_html_e( UDTBP_REQ_WP_VERSION ); ?> version is the minimum requirement to install plugin.</strong>
          <em>( You're running version <?php echo esc_html_e( $wp_version ); ?> )</em>
        </li>
      </ul>
    <?php
        return false;
      }
      return true;
    } // end udtbp_requirements_check()
  } // end class udtbp_Activation
endif;
