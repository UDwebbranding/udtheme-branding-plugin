=== UDTheme Branding ===
Contributors: Christopher Leonard
Tags: branding, higher ed, University of Delaware
Requires at least: 4.3
Tested up to: 4.8.2
Stable tag: 3.1.0
License: GPLv3 or later
License URI: https://opensource.org/licenses/gpl-3.0.html

The plugin allows a University department or college to display the official header and footer on a website.

== Description ==
The UDTheme Branding plugin allows a University department or college to display the official University of Delaware branded header and footer on a website.

Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.

== Installation ==
Manually install
1. Upload the plugin files to your \'wp-content/plugins\' directory.
2. Once uploaded, you may activate or deactivate it from the Plugins menu in your WP admin dashboard.

Automatic install
1. Download the zipped plugin file to your computer.
2. Log in to your WordPress site.
3. Navigate to Plugins > Add New.
4. Browse to the plugin archive and select it.
5. Select Install Now and the plugin will be installed shortly.

== Frequently Asked Questions ==
Who is allowed to use this plugin?
This plugin is ONLY to be used on official University of Delaware department web pages and websites in accordance with CPA guidelines.

Can I display my college name in the header?
Yes. Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.

Can I add department contact information in the footer?
No. Department contact information should be added within the theme\'s footer.

Can I change the color of the branded header?
No. The plugin follows CPA compliance guidelines and therefore users are not allowed to alter the brand.

For questions or concerns, contact the IT Support Center at consult@udel.edu.


== Changelog ==
3.1.0 (October 5, 2017)

* Cleaned up code with more efficiency.
* Added Accessibility features to assist with WCAG 2.0 AA compliance.

3.0.1 (April 1, 2017)

* See issues in repository at https://bitbucket.org/UDwebbranding/udtheme-branding-plugin/issues?status=new&status=open

3.0.0 (January 18, 2017)

* Followed WordPress standards for creating sections and fields
* Cleaned up docs
* Optimized code into a more OOP approach
* Added ability for department admins to add CPA approved custom headers
* Updated branding to comply with new CPA guidelines
* Admin notice displays when incompatible theme is used
* Options saved via AJAX
* Fixed several bugs
