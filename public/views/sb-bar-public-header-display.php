<?php
/**
  * UDTheme Public Header Display
  *
  * This file is used to display the UD branded header on a page.
  *
  * Contains dynamic html that builds the header on the page.
  *
  *
  * @author      Christopher Leonard - University of Delaware | IT CS&S <cleonard@udel.edu>
  * @license     GPL-3.0
  * @link        https://bitbucket.org/UDwebbranding/udtheme-branding-plugin
  * @copyright   Copyright (c) 2012-2016 University of Delaware
  * @version     1.0.0
  * @since       2.0.0
  *
  * @package     udtheme
  * @subpackage  udtheme/public
  *
 */

/**
  * Adding Content to the wp_after_body Hook
  * http://www.internoetics.com/2014/01/02/add-a-hook-in-wordpress-after-the-opening-body-tag/
*/




	wp_reset_query();
	$options = (get_option('sb_bar_options') ? get_option('sb_bar_options') : false);
  if(isset($options["view-header"]) && $options["view-header"] != '') {
    $view_header = $options["view-header"];
  }
  if(isset($options["custom-logo"]) && $options["custom-logo"] != '') {
    //$custom_logo = $options["custom-logo"];
  }
  // print_r(get_option('sb_bar_options'));


print_r($options["custom-logo"]);

//   add_action( 'init', 'test_start_buffer', 0, 0 );

//   function test_start_buffer(){
//       ob_start( 'test_get_buffer' );
//   }

//   function test_get_buffer( $buffer){
//       return preg_replace( '#<body.+>#', '<body>', $buffer);
//       echo 'fuck' . $header_display;
//   }


// function udel_init_theme( $view_header ){


//   if ( $view_header == 'blankHeader' || $view_header == "" ){ return; }
//   $header_display = $this -> header_display();
//   $ob_clean = ob_get_clean();
//   $ob_clean = preg_replace('#<body([^>]*)>#i',"<body$1>{$header_display}",$ob_clean);
//   return $header_display;
// }



	?>
	<header role="banner" id="ud-header" class="flex-container flex-center">
    <div>
    <a title="University of Delaware" href="http://www.udel.edu/" id="udbrand_logo">
    <img alt="University of Delaware" id="ud_primary_logo_big" width="218" height="91" src="<?php echo plugin_dir_path( dirname( __FILE__ ) ) . '/img/logo-udel.png';?>">
    </a>
    <span id="<?php echo $options['url'];?>"><a href="http://www.<?php echo $custom_logo;?>.udel.edu/" aria-label="<?php echo $options['text'];?>"><?php echo $options['text'];?></a></span>
    </div>
  </header>
  <?php


