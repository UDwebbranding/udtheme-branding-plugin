# UDTheme Branding Plugin for WordPress #

Contributors: [Christopher Leonard](http://github.com/atsea)
=======
Requirements: WordPress 4.8 or higher PHP 5.6.30 (current version on CampusPress) or higher

License: GPLv3 or later
License URI: [GPLv3 license](https://opensource.org/licenses/gpl-3.0.html)

## Latest Update ##

10/05/2017

## Description ##

The UDTheme Branding plugin allows a University department or college to display the official University of Delaware branded header and footer on a website.

Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.

### Restrictions ###
This plugin is ONLY to be used on official University of Delaware department web pages and websites in accordance with CPA guidelines.

## Usage ##

### Manually install ###
* Upload the plugin files to your 'wp-content/plugins' directory.
* Once uploaded, you may activate it or deactivate it from the Plugins menu in your WP administration.

### Automatic install ###
* Download the zipped plugin file to your computer.
* Log in to your WordPress site.
* Navigate to Plugins > Add New.
* Browse to the plugin archive and select it.
* Select Install Now and the plugin will be installed shortly.

### Frequently Asked Questions ###
Who is allowed to use this plugin?
This plugin is ONLY to be used on official University of Delaware department web pages and websites in accordance with CPA guidelines.

Can I display my college name in the header?
Yes. Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.

Can I add department contact information in the footer?
No. Department contact information should be added within the theme\'s footer.

Can I change the color of the branded header?
No. The plugin follows CPA compliance guidelines and therefore users are not allowed to alter the brand.

For questions or concerns, contact the IT Support Center at consult@udel.edu.
