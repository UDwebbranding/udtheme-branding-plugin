<div id="saveResult"></div>
<div id="udt_wrap" class="wrap">
	<h1 id="icon-themes" class="udt_dash_icon"><?php echo esc_html_e( get_admin_page_title() ); ?></h1>
  <?php
    //print_r(get_option('udtheme_options'));
    $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'header';
    $this->udtbp_render_tabs();
  ?>
  	<div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-1">
      	<form id="<?php echo esc_attr_e( $this->udtbp.'_form' )?>" method="post" action="options.php">
      		<?php wp_nonce_field( 'udtbp_nonce' );  ?>
        	<div id="postbox-container-2" class="postbox-container">
	        <?php
	        $options = ( get_option( 'udtbp_header_options' ) ? get_option( 'udtbp_header_options' ) : FALSE);
	        $options = ( get_option( 'udtbp_footer_options' ) ? get_option( 'udtbp_footer_options' ) : FALSE);
	        // If no tab or header
	        switch ( $tab ) {
	          case 'footer':
	        ?>
	          <div id="udt_footer_settings" class="postbox">
	            <div class="inside">
	              <?php
                	settings_fields( 'udtbp_footer_options' );
                	do_settings_sections( 'udtbp_footer-footer' );
              	?>
              	<div class="clear"></div>
            	</div>
          	</div>
          	<div class="ud_aligncenter">
            	<button class="button save-button" id="<?php echo esc_attr( 'tab-save' );?>"><?php esc_html_e( 'Save Changes', $this->udtbp ); ?></button>
	          </div>
          <?php
            break;
            case 'about':
            ?>
            <div id="udt_about_settings" class="postbox">
              <div class="inside">
              <h2>About <?php echo UDTBP_NAME; ?> Plugin</h2>
                <p>The <?php echo UDTBP_NAME; ?> plugin allows a University department or college to display the official University of Delaware branded header and footer on a website. The branding plugin is only available for <strong>official University department web pages and websites</strong>.</p>
                <p>Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.</p>
                <div class="clear"></div>
              </div>
            </div>
          <?php
            break;
             case 'support':
            ?>
            <div id="udt_support_settings" class="postbox">
              <div class="inside">
               <h2>Support information</h2>
               <p>If you are experiencing problems with this plugin, contact the Support Center at (302) 831-6000 or <a href="mailto:consult@udel.edu">consult@udel.edu</a>.</p>
               <h3>Frequently Asked Questions</h3>
               <div id="accordion">
               	<h3>Are there any known themes that are incompatible with the plugin?</h3>
                	<div>
                		<p>Yes. <?php echo UDTBP_NAME; ?> is designed to display a University branded header at the top of each page. Some themes contain features that may cause the UD header to 'hide' beneath the menu or cause the branding to display incorrectly. ( <a href="<?php echo esc_url( UDTBP_ADMIN_IMG_URL );?>/incompatible_example.jpg" class="dialogify" data-width="500" data-height="300"><?php esc_html_e( 'See example', $this->udtbp ) ?></a> ).</p>
                		<ul class="support_list">
		                	<!-- <li>Aaron</li>
		                  <li>Anjirai</li>
		                  <li>Boardwalk</li>
		                  <li>Cubic</li>
		                  <li>Divi <span>(Only if fixed navigation is enabled)</span></li>
		                  <li>Highwind</li>
		                  <li>Magazino</li>
		                  <li>Matheson</li>
		                  <li>Radiate</li>
		                  <li>Star</li>
		                  <li>Swell Lite</li>
		                  <li>Temptation</li>
		                  <li>Tracks</li>
		                  <li>Twenty Twelve</li>
		                  <li>Twenty Fourteen</li>
		                  <li>Twenty Fifteen</li>
		                  <li>Twenty Sixteen</li>
		                  <li>Twenty Seventeen</li> -->
									<?php
                		$json_theme_list = json_decode( JSON_THEME_LIST );
                			foreach ( $json_theme_list as $json ){
                  			echo '<li>'.$json.'</li>';
                			}
                	?>
                </ul>
                </div>
                <h3>Who is allowed to use this plugin?</h3>
                  <div>
                    <p>This plugin is ONLY to be used on official University of Delaware department web pages and websites in accordance with CPA guidelines.</p>
                  </div>
                  <h3>Can I display my college name in the header?</h3>
                  <div>
                    <p>Yes. Academic departments and the seven University colleges may choose to use a college-specific logo in addition to the official University header and footer.</p>
                  </div>
                  <h3>Can I add department contact information in the footer?</h3>
                  <div>
                    <p>No. Department contact information should be added within the theme's footer.</p>
                  </div>
                  <h3>Can I change the color of the branded header?</h3>
                  <div>
                    <p>No. The plugin follows CPA compliance guidelines and therefore users are not allowed to alter the brand.</p>
                  </div>
                  <h3>Why am I getting an Undefined error?</h3>
                  <div>
                    <p>If the message below is displayed, try saving your settings again.</p>
                    <img src="<?php echo esc_url( UDTBP_ADMIN_IMG_URL );?>/undefined_example.jpg" class="example_img" width="500" height="213" alt="Undefined message example">
                  </div>
                </div>
              </div>
            </div>
            <div id="dialog" title="Dialog"></div>
          <?php
            break;
            default:
            ?>
          <div id="udt_header_settings" class="postbox">
            <div class="inside">
              <?php
              $options = get_option( 'udtbp_header_options' );
              settings_fields( 'udtbp_header_options' );
              do_settings_sections( 'udtbp_header-header' );
              ?>
              <div class="clear"></div>
            </div>
          </div>
          <div class="ud_aligncenter">
            <button class="button save-button" id="<?php echo esc_attr( 'tab-save' );?>"><?php esc_html_e( 'Save Changes', $this->udtbp ); ?></button>
          </div>
          <?php
            break;
          } // end switch
          ?>
        </div> <!-- end postbox-container-2 -->
        <div class="aligncenter">
          <button class="button save-button" name="save" id="tab-save"><?php esc_html_e( 'Save Changes', $this->udtbp ); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="loading">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4335 4335" width="100" height="100">
    <path fill="#008DD2" d="M3346 1077c41,0 75,34 75,75 0,41 -34,75 -75,75 -41,0 -75,-34 -75,-75 0,-41 34,-75 75,-75zm-1198 -824c193,0 349,156 349,349 0,193 -156,349 -349,349 -193,0 -349,-156 -349,-349 0,-193 156,-349 349,-349zm-1116 546c151,0 274,123 274,274 0,151 -123,274 -274,274 -151,0 -274,-123 -274,-274 0,-151 123,-274 274,-274zm-500 1189c134,0 243,109 243,243 0,134 -109,243 -243,243 -134,0 -243,-109 -243,-243 0,-134 109,-243 243,-243zm500 1223c121,0 218,98 218,218 0,121 -98,218 -218,218 -121,0 -218,-98 -218,-218 0,-121 98,-218 218,-218zm1116 434c110,0 200,89 200,200 0,110 -89,200 -200,200 -110,0 -200,-89 -200,-200 0,-110 89,-200 200,-200zm1145 -434c81,0 147,66 147,147 0,81 -66,147 -147,147 -81,0 -147,-66 -147,-147 0,-81 66,-147 147,-147zm459 -1098c65,0 119,53 119,119 0,65 -53,119 -119,119 -65,0 -119,-53 -119,-119 0,-65 53,-119 119,-119z"
    />
  </svg>
</div>
<div id="udtbp-ajax-saving" class="">
  <img src="<?php echo esc_url( UDTBP_ADMIN_IMG_URL.'/saving-anim.svg' ); ?>" alt="loading" id="loading">
</div>
<div id="udtbp-ajax-message" aria-hidden="true" role="dialog" class=""></div>

