/**
 * ADMIN JAVASCRIPT
 *
 * This file contains all custom jQuery plugins and code used on
 * the plugin admin screens. It contains all of the js
 * code necessary to enable the custom controls used in the live
 * previewer.
 *
 * PLEASE NOTE: The following jQuery plugin dependencies are
 * required in order for this file to run correctly:
 *
 * 1. jQuery      ( http://jquery.com/ )
 *
 * @since   1.4.2
 * @version 1.5.0   Accessible and saving animation scripts added
 */
(function( $ ) {
  'use strict';
  $(function() {
		/**
			* Document ready jQuery function
			*
			* This function contains all plugin related functions.
			*
			*              ['plugin_name']           string      Defines plugin name.
			*              ['udtbp_nonce']           string      Defines plugin nonce for additional security.
			*              ['ajaxurl']               string      Defines the URL for admin-ajax.php which is used for AJAX functionality.
			*              ['view_header']           boolean     TRUE sets value to 1 and displays UD Header.
			*              ['header_custom_logo']    string      Defines College text value displayed.
			*              ['view_footer']           string      TRUE sets value to 1 and displays UD Footer.
			*              ['footer_color']          string      Defines color of UD Footer lockup logo and social icons.
			*              ['udtbp_theme_override']  string      Defines CSS overrides for currently active problem themes.
			*              ['udtheme_admin_js_vars'] string      Prepended to above for use in public facing JS file.
		*/
	var groupInput = $('.grey-main input[type="radio"]');
	var hdstateinput = $('#header-state input[type="radio"]');
	var notify = $('p.notify');

	var plugin_name = udtheme_admin_js_vars.plugin_name;
	var Header = udtheme_admin_js_vars.Header;
	var header_id = udtheme_admin_js_vars.header_id;
	var header_custom_logo = udtheme_admin_js_vars.header_custom_logo;
	var Footer = udtheme_admin_js_vars.Footer;
	var footer_id = udtheme_admin_js_vars.footer_id;
	var footer_color = udtheme_admin_js_vars.footer_color;
	var block = udtheme_admin_js_vars.block;
	var blank = udtheme_admin_js_vars.blank;
	var hdblank = $( header_id ).add( blank, Header );
	var hdblock = $('#' + header_id + block + 'Header');
	var switch_selection = $('.switch-selection');
	var footerText = 'Footer';
	var ftblank = $('#' + plugin_name + '-blankFooter');
	var ftblock = $('#' + plugin_name + '-blockFooter');
	var ftcolorh3 = $('#footer-color h3 ');
	var ftcolorsmall = $('#footer-color small');
	var ftcolor = $('#footer-color');
	var ftstateinput = $('#footer-state > input');
	var ftcolorinput =$('#footer-color > input');//input[name="sb_bar_footer_options[color-footer]"]');
	var ftError ='';

	var subbut = $('#ud_form input[type="submit"]');
	var Off = 'off';
	var On = 'on';
	// get localStorage value of footer color

	$(function () {
	    var data = localStorage.getItem("items");
	    localStorage.removeItem('setHeader');

	    if (data != null) {
	        ftcolorinput.prop("disabled", true);
	    }
	    console.log(data);
	    console.log('header state' + data_header_state);
	});

	/**
	 * STORE PLUGIN OPTIONS IN ARRAY IN LOCALSTORAGE
	 * setClass 	convert setClass to JSON
	 *
	 * @type 			string
	 * @link      http://stackoverflow.com/questions/3357553/how-to-store-an-array-in-localstorage
	 */
	var setClass = JSON.parse(localStorage.getItem('setClass')) || {};
	$.each(setClass, function () {
	    $(this.selector).addClass(this.className);
	});
	var addClassToLocalStorage = function(selector, className) {
	    setClass[selector + ':' + className] = {
	        selector: selector,
	        className: className
	    };
	    localStorage.setItem('setClass', JSON.stringify(setClass));
	};
	var removeClassFromLocalStorage = function(selector, className) {
	    delete setClass[selector + ':' + className];
	    localStorage.setItem('setClass', JSON.stringify(setClass));
	};
	/**
	 * GET VALUE OF HEADER RADIO BUTTON CHOICE
	 * setClass 	convert setClass to JSON
	 *
	 * @type 			string
	 * @link      http://stackoverflow.com/questions/8622336/jquery-get-value-of-selected-radio-button
	 */
	// hdstateinput.on('change',function(){
	//   var selectedVal = "";
	//   var selected = $("#header-state input[type='radio']:checked");
	//   if (selected.length > 0) {
	//       selectedVal = selected.val();
	//   }

	//   if( selectedVal == 'blankHeader') {
	//     header_id = NULL;
	//     $('#header-state, .switch-selection').addClass('blank').removeClass('block');
	//     notify
	//       .text('You have turned ' + Off + ' the UD '+ Header.toLowerCase() + '. Click Save Changes for the setting to take effect.')
	//       .addClass('red')
	//       .removeClass('green slideup');
	//     setTimeout(function(){
	//       notify.addClass('slideup');
	//     }, 3000);
	//     $('.notice.notice-warning.is-dismissible').fadeOut('slow');
	//    //  $('.switch-label.switch-label-off').text(function(_, text) {
	//    //    return text === 'Disable' ? 'Disabled' : 'Disable';
	//     // });
	//     localStorage.removeItem('setHeader');
	//   }
	//   else {
	//     $('#header-state, .switch-selection').addClass('block').removeClass('blank');
	//     notify
	//    .text('You have turned ' + On + ' the UD '+ Header.toLowerCase() + '. Click Save Changes for the setting to take effect.')
	//         .addClass('green')
	//         .removeClass('red slideup');
	//     setTimeout(function(){
	//         notify.addClass('slideup');
	//     }, 3000);
	//     $('.notice.notice-warning.is-dismissible').fadeIn('slow');
	//     // addClassToLocalStorage(switch_selection, 'block');
	//     // removeClassFromLocalStorage(switch_selection, 'blank');
	//     localStorage.setItem('setHeader', 1);
	//   }
	// }); // end header-state on()
	//footer control for text changes, on/off notifications and radio slide
	// ftstateinput.on('change',function(){
	//     // http://stackoverflow.com/questions/8622336/jquery-get-value-of-selected-radio-button
	//     var ft_state_selectedValue = "";
	//     var ft_state_selected = $("#footer-state input[type='radio']:checked");
	//     var ft_color_selectedValue = "";
	//     var ft_color_selected = $("#footer-color input[type='radio']:checked");
	//     //var selected = $("#footer-state input[type='radio']:checked");
	//     if (ft_state_selected.length > 0) {
	//         ft_state_selectedValue = ft_state_selected.val();
	//     }
	//     if (ft_color_selected.length > 0) {
	//         ft_color_selectedValue = ft_state_selected.val();
	//     }
	//     else {
	//       ft_color_selectedValue == null;
	//     }
	//     if( ft_state_selectedValue == 'blankFooter') {

	//       $('#header-state, .switch-selection').addClass('block').removeClass('blank');
	//       ftcolorinput.prop('checked', false).prop('disabled',true).addClass('disabled');

	//       notify
	//         .text('You have turned ' + Off + ' the UD '+ Footer.toLowerCase() + '. Click Save Changes for the setting to take effect.')
	//         .addClass('red')
	//         .removeClass('green slideup');
	//       setTimeout(function(){
	//         notify.addClass('slideup');
	//       }, 3000);
	//       // addClassToLocalStorage(switch_selection, 'blank');
	//       // removeClassFromLocalStorage(switch_selection, 'block');
	//       localStorage.removeItem('setFooter');
	//       //localStorage.removeItem('setColorDiv', array(ftcolorinput.val(), ftcolorinput ) );

	//       var items = [];
	//       items[0] = ft_color_selectedValue == '';
	//       items[1] = ftcolorinput.prop('disabled',true);
	//       localStorage.setItem("items", JSON.stringify(items));

	//       //var storeItems = JSON.parse(localStorage.getItem("items"));
	//     }
	//     else {
	//           var ftError = '';
	//           if (!ftcolorinput.is(':checked') ){
	//             ftError = 'Choose "Text and images color"';
	//           }
	//           ftcolorinput.prop('disabled',false).removeClass('disabled');

	//           checkColor();

	//           switch_selection.addClass('block').removeClass('blank');
	//           notify
	//          .text('You have turned ' + On + ' the UD '+ Footer.toLowerCase() + ' ' + ftError +'. Click Save Changes for the setting to take effect.')
	//               .addClass('green')
	//               .removeClass('red slideur1p');
	//           setTimeout(function(){
	//               notify.addClass('slideup');
	//           }, 3000);
	//       // addClassToLocalStorage(switch_selection, 'block');
	//       // removeClassFromLocalStorage(switch_selection, 'blank');
	//       //localStorage.setItem('setColorDiv', array(ftcolorinput.val(), ftcolorinput.prop('disabled',false) ) );//, ftcolorinput.prop('disabled',true));
	//       localStorage.setItem('setFooter', 1);

	//       var items = [];
	//       items[0] = ftcolorinput.val();
	//       items[1] = ftcolorinput.prop('disabled',false);
	//       items[2] = 'setColorDiv';
	//       localStorage.removeItem("items", JSON.stringify(items));


	//     }
	//   }); // end footer state on()
	  // footer color option control
	  function checkColor() {
	      ftcolorinput.on('click', function() {
	          ftError = '';
	      });
	      return true;
	  }
	  /**
	   * @param  {[type]}
	   * @return {[type]}
	   */
	  $('.box-content').on( 'click', '.udt_yes_no_button', function(e){
	    e.preventDefault();

	    var $click_area = $(this),
	      $box_content              = $click_area.parents('.box-content'),
	      $checkbox                 = $box_content.find('input[type="checkbox"]'),
	      $state                    = $box_content.find('.udt_yes_no_button');
	      $ftcoloroption            = $box_content.find('ftcolorinput');
	      //$header_custom_logo       = $box_content.find('#sb_bar_options[custom-logo]');

	    $state.toggleClass('udt_on_state udt_off_state');


	    if ( $checkbox.is(':checked' ) ) {
	      // check if header visibility is not checked
	      $checkbox.prop('checked', false, function(){
	        if( $('#udtbp_theme_override').is(':visible')){
	          $( '#udtbp_theme_override' ).addClass( 'hide').fadeOut( 'fast' );
	        }
	      });

	    } else {
	      $checkbox.prop('checked', true);
	      $( '#udtbp_theme_override' ).removeClass( 'hide').fadeIn( 'fast' );
	    }
	    var ftError = '';
	    if (!$ftcoloroption.is(':checked') ){
	      ftError = 'Choose "Text and images color"';
	    }
	    $ftcoloroption.prop('disabled',false).removeClass('disabled');
	  });  // end box-content on()
	  $(' #udt_header_settings select ').on( 'click', function(){
	    $( this ).parent( 'label' ).toggleClass( 'focus' );
	  });
  	/**
	   	* SAVE VIA AJAX
			*
			* Uses options instead of admin-ajax.
			*
			* @since  3.0.0
			* @example https://stackoverflow.com/questions/10873537/saving-wordpress-settings-api-options-with-ajax
			* @example https://www.wpoptimus.com/434/save-plugin-theme-setting-options-ajax-wordpress/
	  */

	  function save_main_options_ajax() {
	   	$('#udtbp-ajax-saving').hide();
	   	$('#udtbp_theme_override').removeClass('hide').fadeIn( 'fast');
	   	$('#udtbp_form').submit( function () {
   			$('#udtbp-ajax-saving' ).fadeIn('fast');
   			var b =  $(this).serialize();

   			$.post(
       		'options.php', b
   		  )
   			.done(function() {
   				console.log('success ');
   				$('#udtbp-ajax-message')
 					.css('background-color', '#090')
 					.html('<p class="modal_header" tabindex="0">Settings saved successfully</p>')
 					.fadeIn('slow', function() {
         		$(this).delay(700).fadeOut('slow');
 			    });
   			})
   			.fail(function(response) {
   				console.log('error');
	  				$('#udtbp-ajax-message')
						.css('background-color', '#090')
						.html('<p class="modal_header" tabindex="0">There was an error and Settings were not saved</p>')
						.fadeIn('slow', function() {
	        		$(this).delay(700).fadeOut('slow');
				    });
   				})
   		.always(function() {
   			$( '#udtbp-ajax-saving' ).hide();
   		});
   		return false;
    	}); // end submit()
  	} // end save_main_options_ajax()
  	save_main_options_ajax();

  	/**
  	 * SUPPORT TAB DIALOG
  	 * @example  https://github.com/salmanarshad2000/demos/blob/v1.0.0/jquery-ui-dialog/size-to-fit-content.html
  	 */
  	  function udtbp_dialog() {
  	    $('#dialog').dialog({
  	      autoOpen: false,
  	      resizable: false,
  	      title: 'Example of fixed navigation',
  	      width: 'auto',
  	      'closeOnEscape' : true,
  	      show: {
  	        effect: 'fade',
  	        duration: 1000
  	      },
  	      hide: {
  	        effect: 'fade',
  	        duration: 1000
  	      }
  	    }); // end dialog()
  	    $('.dialogify').on('click', function(e) {
  	      e.preventDefault();
  	      $('#dialog').html("<img src='" + $(this).prop("href") + "' width='" + $(this).attr("data-width") + "' height='" + $(this).attr("data-height") + "'>");
  	      $('#dialog').dialog(
  	        'option',
  	        'position', {
  	          my: 'center',
  	          at: 'center',
  	          of: window
  	      }); // end dialog()
  	      if ($('#dialog').dialog('isOpen') == false) {
  	        $('#dialog').dialog('open');
  	      }
  	    });
  	  } // end udtbp_dialog()
	}) // end $(function()
}) ( jQuery );


(function ($) {
  'use strict';
  /**
    Document ready jQuery function
    *
    This function contains all plugin related functions.
    *
    *              ['plugin_name']           string      Defines plugin name.
    *              ['udtheme_admin_js_vars'] string      Prepend to above for use in public facing JS file.
    */



  hdstateinput.on('change',function(){
    // http://stackoverflow.com/questions/8622336/jquery-get-value-of-selected-radio-button
    var selectedVal = "";
    var selected = $("#header-state input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    }

    if( selectedVal == 'blankHeader') {
      header_id = NULL;
      // $('#header-state, .switch-selection').addClass('blank').removeClass('block');
      // notify
      //   .text('You have turned ' + Off + ' the UD '+ Header.toLowerCase() + '. Click Save Changes for the setting to take effect.')
      //   .addClass('red')
      //   .removeClass('green slideup');
      // setTimeout(function(){
      //   notify.addClass('slideup');
      // }, 3000);
      $('.notice.notice-warning.is-dismissible').fadeOut('slow');
      localStorage.removeItem('setHeader');
    }
    else {
      $('.notice.notice-warning.is-dismissible').fadeIn('slow');
      localStorage.setItem('setHeader', 1);
    }
  }); // end header-state on()
  //footer control for text changes, on/off notifications and radio slide
  ftstateinput.on('change',function(){
      // http://stackoverflow.com/questions/8622336/jquery-get-value-of-selected-radio-button
      var ft_state_selectedValue = "";
      var ft_state_selected = $("#footer-state input[type='radio']:checked");
      var ft_color_selectedValue = "";
      var ft_color_selected = $("#footer-color input[type='radio']:checked");
      //var selected = $("#footer-state input[type='radio']:checked");
      if (ft_state_selected.length > 0) {
          ft_state_selectedValue = ft_state_selected.val();
      }
      if (ft_color_selected.length > 0) {
          ft_color_selectedValue = ft_state_selected.val();
      }
      else {
        ft_color_selectedValue == null;
      }
      if( ft_state_selectedValue == 'blankFooter') {

        $('#header-state, .switch-selection').addClass('block').removeClass('blank');
        ftcolorinput.prop('checked', false).prop('disabled',true).addClass('disabled');

        notify
          .text('You have turned ' + Off + ' the UD '+ Footer.toLowerCase() + '. Click Save Changes for the setting to take effect.')
          .addClass('red')
          .removeClass('green slideup');
        setTimeout(function(){
          notify.addClass('slideup');
        }, 3000);
        // addClassToLocalStorage(switch_selection, 'blank');
        // removeClassFromLocalStorage(switch_selection, 'block');
        localStorage.removeItem('setFooter');
        //localStorage.removeItem('setColorDiv', array(ftcolorinput.val(), ftcolorinput ) );

        var items = [];
        items[0] = ft_color_selectedValue == '';
        items[1] = ftcolorinput.prop('disabled',true);
        localStorage.setItem("items", JSON.stringify(items));

    var udtheme_admin_js_vars = '';
    var plugin_name = udtheme_admin_js_vars.plugin_name;
    /**
     * CHECKBOX AND RADIO STATES
     *
     * Derived from Divi Theme JS
     */
    $( '.box-content' ).on( 'click', '.udt_yes_no_button', function( e ){
      e.preventDefault();

      var $click_area = $( this ),
      $box_content              = $click_area.parents( '.box-content' ),
      $checkbox                 = $box_content.find( 'input[type="checkbox"]' ),
      $state                    = $box_content.find( '.udt_yes_no_button' );
      if ( $checkbox.is( ':checked' ) ) {
        // is not checked
        $( '#udtbp_theme_override' ).addClass( 'ud_hidden' ).fadeOut( 'fast' );
      }
      else {
        // is checked
        $( '#udtbp_theme_override' ).removeClass( 'ud_hidden' ).fadeIn( 'fast' );
      }
      $state.toggleClass( 'udt_on_state udt_off_state' );
      if ( $checkbox.is( ':checked' ) ) {
        // check if header visibility is not checked
        $checkbox.prop('checked', false);
      } else {
        $checkbox.prop('checked', true);
      }
    });  // end box-content on()
    /**
     * SELECT STATES
     *
     * Add focus class on dropdown label
     */
    $( ' #udt_header_settings select ' ).on( 'click', function(){
      $( this ).parent( 'label' ).toggleClass( 'focus' );
    });
});

$(function () {




    /**
      * JQUERY UI ACCORDION
      * @example  https://jqueryui.com/accordion/#collapsible
    */
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false,
      icons: icons
    });
    /**
      * SUPPORT TAB DIALOG
      * @example  https://github.com/salmanarshad2000/demos/blob/v1.0.0/jquery-ui-dialog/size-to-fit-content.html
    */
    $('#dialog').dialog({
      autoOpen: false,
      resizable: false,
      title: 'Example of fixed navigation',
      modal:true,
      width: 'auto',
      'closeOnEscape' : true,
      show: {
        effect: 'fade',
        duration: 1000
      },
      hide: {
        effect: 'fade',
        duration: 1000
      }
    }); // end dialog()
    $('.dialogify').on('click', function(e) {
      var dialogwin = $('#dialog');
      var container = $('.ui-dialog');
      e.preventDefault();
      dialogwin.html("<img src='" + $(this).prop("href") + "' width='" + $(this).attr("data-width") + "' height='" + $(this).attr("data-height") + "'>");
      dialogwin.dialog(
        'option',
        'position', {
        my: 'center',
        at: 'center',
        of: window
      }); // end dialog()
      if (dialogwin.dialog('isOpen') === false) {
        dialogwin.dialog('open');
      }
      /**
       * Close dialog and hide overlay
       * Used to close popup when clicking outside dialog.
       */
      $( '.ui-widget-overlay' ).on( 'click', function ( e ) {
        e.preventDefault();
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          dialogwin.dialog('close');
        }
      });
    });
  }) //end ready function use strict
}) ( jQuery );


/**
 * FEATURE DETECTION CHECK FOR MS EDGE
 *
 * @example https://mobiforge.com/design-development/html5-pointer-events-api-combining-touch-mouse-and-pen
 */
 if (window.PointerEvent) {
  //alert(' Pointer events are supported.');
}
