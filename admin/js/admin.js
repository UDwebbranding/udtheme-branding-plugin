(function( $ ) {
  'use strict';

  $(function() {
    var $save_message = $("#epanel-ajax-saving");
    var ajaxurl = udtheme_admin_js_vars.ajaxurl;
    $('#submit').click(function(){
        udtheme_save( false, true );
        return false;
    });

    function udtheme_save( callback, message ) {
      var plugin_options = $('#sb_bar_form').serialize();
      add_nonce = '&_ajax_nonce='+udtheme_admin_js_vars.udt_nonce;

      plugin_options += add_nonce;
//udtheme_settings_save

      $.ajax({
        type: "POST",
        url: ajaxurl,
        // data:{
        //                 action: 'udtheme_settings_save_ajax',

        //                 security: udtheme_admin_js_vars.udt_nonce,
        //             },
        // dataType: 'json',
        data: plugin_options,
        beforeSend: function ( xhr ){
          if ( message ) {
            $save_message.removeAttr('class').fadeIn('fast');
          }
        },
        success: function(response){
          console.log(response);
          if ( message ) {
            $save_message.addClass('success-animation');

            setTimeout(function(){
              $save_message.fadeOut();
            },500);
          }

          if ( $.isFunction( callback ) ) {
            callback();
          }
        }

      });
    }
  });
})( jQuery );
