  jQuery(document).ready(function($){
    // ":not([safari])" is desirable but not necessary selector
    // ":not([safari])" is desirable but not necessary selector
    $('#epanel input:checkbox:not([safari]):not(.yes_no_button)').checkbox();
    $('#epanel input[safari]:checkbox:not(.yes_no_button)').checkbox({cls:'jquery-safari-checkbox'});
    $('#epanel input:radio:not(.yes_no_button)').checkbox();

    // Yes - No button UI
    $('.yes_no_button').each(function() {
      $checkbox = $(this),
      value     = $checkbox.is(':checked'),
      state     = value ? 'et_pb_on_state' : 'et_pb_off_state',
      //$template = $($('#epanel-yes-no-button-template').html()).find('.et_pb_yes_no_button').addClass(state);

      $checkbox.hide().after($template);
    });

    $('.box-content').on( 'click', '.et_pb_yes_no_button', function(e){
      e.preventDefault();

      var $click_area = $(this),
        $box_content = $click_area.parents('.box-content'),
        $checkbox    = $box_content.find('input[type="checkbox"]'),
        $state       = $box_content.find('.et_pb_yes_no_button');

      $state.toggleClass('et_pb_on_state et_pb_off_state');

      if ( $checkbox.is(':checked' ) ) {
        $checkbox.prop('checked', false);
      } else {
        $checkbox.prop('checked', true);
      }

    });

    var $save_message = $("#epanel-ajax-saving");

    $('#epanel-save-top').click(function(e){
      e.preventDefault();

      $('#epanel-save').trigger('click');
    })

    $('#epanel-save').click(function(){
      epanel_save( false, true );
      return false;
    });

    function epanel_save( callback, message ) {
      var options_fromform = $('#main_options_form').formSerialize(),
        add_nonce = '&_ajax_nonce='+ePanelSettings.epanel_nonce;

      options_fromform += add_nonce;

      $.ajax({
        type: "POST",
        url: ajaxurl,
        data: options_fromform,
        beforeSend: function ( xhr ){
          if ( message ) {
            $save_message.removeAttr('class').fadeIn('fast');
          }
        },
        success: function(response){
          if ( message ) {
            $save_message.addClass('success-animation');

            setTimeout(function(){
              $save_message.fadeOut();
            },500);
          }

          if ( $.isFunction( callback ) ) {
            callback();
          }
        }
      });
    }
  });
