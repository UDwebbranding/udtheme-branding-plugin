<?php
/**
  * Class: UDTheme Branding Admin
  *
  * The purpose of this class is to:
  * Enqueue admin specific styles and scripts
  * Define admin and public facing hooks
  * Register plugin settings in admin dashboard
  * Validate and sanitize plugin options
  * Render plugin settings tabs
  * Display admin specific notices
  * Add settings link to plugin.php
  * Display admin html
  *
  * @package     udtheme-brand
  * @subpackage  udtheme-brand/admin
  * @author      Christopher Leonard
  * @license     GPLv3 or Later
  * @copyright   Copyright (c)2012-2017 Christopher Leonard University of Delaware
  * @version     3.1.0
*/
if ( ! class_exists( 'udtbp_Admin' ) ) :
  class udtbp_Admin {
  /**
   * The ID of this plugin.
   *
   * @since    1.4.2
   * @version  1.0.0                           New name introduced.
   * @access   private
   * @var      string         $udtbp           The ID of this plugin.
  */
   private $udtbp;
   /**
   * The current active theme.
   *
   * @since    3.0.0
   * @access   private
   * @var      string          $current_theme    The current theme.
  */
   private $current_theme;
  /**
   * Slug of the plugin screen.
   *
   * @since    3.0.0
   * @access   protected
   * @var      string          $plugin_screen_hook_suffix  The plugin options screen.
  */
   protected $plugin_screen_hook_suffix = null;
  /**
   * Plugin tabs array.
   *
   * @since    3.0.0
   * @access   public
   * @var      array            $plugin_settings_tabs    Array used for each tab in dashboard
  */
   public $plugin_settings_tabs = array();
  /**
   * Incompatible themes list array.
   *
   * @since    3.0.0
   * @access   public
   * @var      array            $issues_theme_name    Array for each theme by name.
  */
   public static $issues_theme_name = array();
  /**
   * Incompatible themes list array.
   *
   * @since    3.0.0
   * @access   public
   * @var      array            $issues_theme_json    Array for each theme by name.
  */
   public $issues_theme_json;
	/**
	 * CLASS INITIALIZATION
   * Initiates the class and set its properties.
   *
   * @since      3.0.0
   * @param      string           $udtbp             The ID of this plugin.
   * @param      string           $current_theme     The current active theme.
  */
   public function __construct( $udtbp, $current_theme ) {
     $this->udtbp = $udtbp;
     $this->current_theme = wp_get_theme();
     $this->plugin_settings_tabs['header']  = 'Header';
     $this->plugin_settings_tabs['footer']  = 'Footer';
     $this->plugin_settings_tabs['about']   = 'About';
     $this->plugin_settings_tabs['support'] = 'Support';
   }

  /**
   * ENQUEUE ADMIN CSS
   * This function is used to:
   * Register and enqueue admin-specific stylesheets
   * Adds admin specific stylesheets files.
   *
   * @since       3.0.0
   * @version     1.0.1                  Updated @example to @link
   * @param       string      $screen    Used to get the name of the screen that the current user is on.
   * @return      null                   Return early if no settings page is registered.
   * @link     http://wordpress.stackexchange.com/questions/195864/most-elegant-way-to-enqueue-scripts-in-function-php-with-foreach-loop
   * @link     http://wordpress.stackexchange.com/questions/145782/check-and-dequeue-if-multiple-stylesheets-exists-using-wp-style-is
   */
   public function enqueue_styles( $screen ) {
    if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
      return;
    }

    $screen = get_current_screen();
    if ( $this->plugin_screen_hook_suffix == $screen->id ) {
      wp_deregister_style( $this->udtbp .'-admin-styles' );
      wp_register_style( $this->udtbp .'-admin-styles', UDTBP_ADMIN_CSS_URL.'/udtbp-admin.css', array(), UDTBP_VERSION, 'all' );
      wp_enqueue_style( $this->udtbp .'-admin-styles' );
      wp_enqueue_style( 'wp-jquery-ui-dialog' );
    }
   } // end enqueue_styles()
  /**
   * ENQUEUE ADMIN CSS FOR IE
   * This function is used to:
   * Register and enqueue admin-specific CSS styles for Internet Explorer.
   *
   * @since       3.0.0
   * @uses        $is_IE      var for IE < Edge
   * @uses        $is_edge    var for Edge
   * @param       string      $plugin_screen_hook_suffix
   * @param       string      $screen    Used to get the name of the screen that the current user is on.
   * @link     http://stackoverflow.com/questions/21277190/wordpress-admin-bar-not-showing-on-frontend
  */
   public function enqueue_styles_ie( $screen ) {
    if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
      return;
    }

    $screen = get_current_screen();
    if ( $this->plugin_screen_hook_suffix == $screen->id ) {
      global $is_IE;
      global $is_edge;
      if( ( $is_IE ) || ( $is_edge ) ) {
        wp_register_style( $this->udtbp .'-admin-styles-ie', UDTBP_ADMIN_CSS_URL.'/udtbp-ie-admin.css', array(), UDTBP_VERSION, 'all' );
        wp_enqueue_style( $this->udtbp .'-admin-styles-ie' );
      }
     }
   } // end enqueue_styles_ie()
  /**
   * ADD ADMIN INLINE CSS
   * Check if WP Admin Bar is showing and adds inline styles
   * to override WP admin-bar.min.css so that bar doesn't
   * cover UDBrand header.
   *
   * @since 3.0.0
  */
   public function udtbp_enqueue_inline_admin_styles() {
    if ( is_user_logged_in() && is_admin_bar_showing() ) :
      $custom_css =
       '@media screen and (max-width:600px) {#wpadminbar {position: fixed !important;}div#post-body.metabox-holder.columns-1 {overflow-x: visible !important;}}'."\n";
       '@media only screen and (max-width:48em) {#wpadminbar {position:fixed !important;}}'."\n";
       wp_add_inline_style( $this->udtbp .'-admin-styles', $custom_css );
    endif;
   } // end udtbp_enqueue_inline_admin_styles()


	/**
	 * ADD ADMIN INLINE CSS
 	 * Check if WP Admin Bar is showing and adds inline styles
	 * to override WP admin-bar.min.css so that bar doesn't
	 * cover UDBrand header.
	 *
	 * @since 3.0.0
	*/
	 public function udtbp_enqueue_inline_admin_styles() {
	  if ( is_user_logged_in() && is_admin_bar_showing() ) :
		  $custom_css =
			 '@media screen and (max-width:600px) {#wpadminbar {position: fixed !important;}div#post-body.metabox-holder.columns-1 {overflow-x: visible !important;}}'."\n";
			 '@media only screen and (max-width:48em) {#wpadminbar {position:fixed !important;}}'."\n";
			 wp_add_inline_style( $this->udtbp .'-admin-styles', $custom_css );
		endif;
	 } // end udtbp_enqueue_inline_admin_styles()
   /**
    * ENQUEUE WORDPRESS CORE JAVASCRIPT
    *
    * JQUERY SCRIPTS LOADED CHECK
    *
    * Checks to see if jquery, jquery-form, jquery-ui-core,
    * jquery-ui-dialog, jquery-ui-tabs, jquery-ui-widget are loaded
    *
    * Build an array of scripts to enqueue
    * key = script handle
    *
    * @since    3.0.0
    * @param    array     $jquery_addons_js_check       Dependent scripts array
    * @param    string    $min                          If not in debug mode load minified version.
    * @return   null                                    Return early if no settings page is registered.
    * @uses               jQuery Form, jQuery UI (Dialog,Tabs, Widget, Accordion) scripts
    * @link  http://wordpress.stackexchange.com/questions/195864/most-elegant-way-to-enqueue-scripts-in-function-php-with-foreach-loop
    * @link  https://wordpress.org/support/topic/broken-dependencies-1/
   */

     public function enqueue_core_scripts( $screen ) {
      if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
        return;
      }
      $screen = get_current_screen();
      if ( $this->plugin_screen_hook_suffix == $screen->id ) {

        $min = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
         // $jquery_addons_js_check = [
         //   'jquery-form'       => includes_url( 'js/jquery/form{$min}.js' ),
         //   'jquery-ui-core'    => includes_url( 'js/jquery/ui/core{$min}.js' ),
         //   'jquery-ui-dialog'  => includes_url( 'js/jquery/ui/dialog{$min}.js' ),
         //   'jquery-ui-tabs'    => includes_url( 'js/jquery/ui/tabs{$min}.js' ),
         //   'jquery-ui-widget'  => includes_url( 'js/jquery/ui/widget{$min}.js' ),
         // ];
         //   array( implode( ",", $jquery_js_check ) );
        $jquery_addons_js_check = [
          'jquery-ui-accordion',
          'jquery-form',
          'jquery-ui-core',
          'jquery-ui-dialog',
          'jquery-ui-tabs',
          'jquery-ui-widget',
        ];

        foreach ( $jquery_addons_js_check as $key ) {
         // Check if the script is enqueued.
         if ( wp_script_is( $key, 'enqueued' ) ) {
           return;
         }
         else {
           wp_enqueue_script( 'jquery-ui-tabs' );
           wp_enqueue_script( 'jquery-ui-accordion' );
         }
        } //end foreach
      }
    } //end enqueue_core_scripts()
  /**
   * ENQUEUE ADMIN JAVASCRIPT
   * This function is used to:
   * Register and enqueue admin-specific javascript
   * Adds admin specific javascript files.
   *
   * @since       3.0.0
   * @param       string      $screen    Used to get the name of the screen that the current user is on.
   * @return      null                   Return early if no settings page is registered.
   * @uses        jQuery Form, jQuery UI Dialog, jQuery UI Tabs, jQuery UI Widget scripts
  */
   public function enqueue_scripts( $screen ) {
    if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
      return;
    }
    $screen = get_current_screen();
    if ( $this->plugin_screen_hook_suffix == $screen->id ) {
      wp_enqueue_script( 'jquery-ui-tabs' );
      wp_deregister_script( $this->udtbp . '-admin-script' );
      wp_register_script( $this->udtbp .'-admin-script', UDTBP_ADMIN_JS_URL.'/udtbp-admin.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-dialog', 'jquery-form' ), UDTBP_VERSION, TRUE );
      wp_enqueue_script( $this->udtbp . '-admin-script' );

      $args_localize_script = [
        'plugin_name' => $this->udtbp,
        'udtbp_nonce' => wp_create_nonce( $this->udtbp.'_nonce' ),
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'view_header' => $this->udtbp.'_options[view-header]',
        'header_custom_logo' => $this->udtbp.'_options[custom-logo]',
        'view_footer' => $this->udtbp.'_options[view-footer]',
        'footer_color' => $this->udtbp.'_options[color-footer]',
        'udtbp_theme_override' => $this->udtbp.'_theme_override'
      ];
      wp_localize_script( $this->udtbp . '-admin-script', 'udtheme_admin_js_vars', $args_localize_script );
    }
  } // end enqueue_scripts()
  /**
   * REGISTER SETTINGS PAGE
   *
   * @since     3.0.0
   * @version   1.1.0                  Added @constant
   * @var       constant   UDTBP_NAME  UDTheme Branding text string as constant.
   * @var       string     $page_title Title used on Options page for plugin.
   * @var       string     $menu_title Title used on Settings menu for plugin.
   * @var       string     $capability Tasks that user is allowed to perform.
   * @var       string     $menu_slug  Slug name to refer to this menu by.
   * @var       callable   $function   Callback function used to output content display_plugin_admin_page().
   * @link   https://coderwall.com/p/lw2suw/enqueue-scripts-styles-on-a-wordpress-option-page
  */
	 public function udtbp_admin_menu() {
	   $this->plugin_screen_hook_suffix =
		  add_options_page(
        __( UDTBP_NAME . ' Settings', $this->udtbp ),
        __( UDTBP_NAME ),
        'manage_options',
        $this->udtbp,
        array( $this, 'display_plugin_admin_page' )
      );
   } // end function udtbp_admin_menu()
  /**
   * SANITIZE SETTINGS
   * Validates saved options
   *
   * @since     3.0.0
   * @param     array     $new_input  Initialize new array that will hold the sanitize values
   * @param     array     $input      array of submitted plugin options. Loop through and sanitize values
   * @return    array                 array of validated plugin options
  */
   public function settings_sanitize( $input ) {
    $new_input = array();
    if(isset($input)) :
      foreach ( $input as $key => $val ) {
       if( $key == 'post-type' ) { // dont sanitize array
         $new_input[ $key ] = $val;
       }
       else {
         $new_input[ $key ] = sanitize_text_field( $val );
       }
      }
    endif;
    return $new_input;
   } // end settings_sanitize()
  /**
   * AJAX SAVE SETTINGS
   *
   * @since     3.0.0
  */
//  public function udtbp_save_ajax() {
//   check_ajax_referer( 'udtbp_nonce' );
//   epanel_save_data( 'ajax' );

//   die();
// }
  // public function udtbp_settings_save_ajax() {
  //   check_ajax_referer('_wpnonce', '_wpnonce' );
  //   $data = $_POST;
  //   unset( $data['option_page'], $data['action'], $data['_wpnonce'], $data['_wp_http_referer'] );
  //   if ( update_option('main_options', $data ) ) {
  //     die(1);
  //     echo 'SUCCESS!';
  //   }
  //   else {
  //     die (0);
  //   }
  // }
  public function udtbp_save_ajax() {
    check_ajax_referer('_wpnonce', 'udtbp_nonce' );
    //echo json_encode( $_POST );
      if ( isset( $_POST['action'] ) && $_POST['action'] == 'udtbp_save_ajax' ) {
        // Do the saving
        $options = ( get_option( 'udtbp_header_options' ) ? get_option( 'udtbp_header_options' ) : FALSE );
        $options = ( get_option( 'udtbp_footer_options' ) ? get_option( 'udtbp_footer_options' ) : FALSE );

        if ( ! empty( $options['view-header'] ) ) {
          $option = $options['view-header'];
        }
        else {
          $options['view-header'] = NULL;
        }
        $options = get_option('plugin_shrinkylink_settings');

        $options['comments'] = 'new value';
        $options['posts'] = 'new value';
        $options['size'] = 'new value';
        $options['scheme'] = 'new value';
        $options['www'] = 'new value';

        update_option('plugin_shrinkylink_settings', $options);


        $args_ajax_options = array(
          'view_header' => $this->udtbp.'_options[view-header]',
          'header_custom_logo' => $this->udtbp.'_options[custom-logo]',
          'view_footer' => $this->udtbp.'_options[view-footer]',
          'footer_color' => $this->udtbp.'_options[color-footer]'
        );
        update_option($this->udtbp . '-admin-options', $args_ajax_options );

        $view_header     = ( isset( $_POST['view_header'] ) ) ? sanitize_text_field( $_POST['view_header'] ) : false;
        $view_footer     = ( isset( $_POST['view_footer'] ) ) ? sanitize_text_field( $_POST['view_footer'] ) : false;
        $header_custom_logo   = ( isset( $_POST['header_custom_logo'] ) ) ? esc_attr( $_POST['header_custom_logo'] ) : false;
        $footer_color = ( isset( $_POST['footer_color'] ) ) ? sanitize_textarea_field( $_POST['footer_color'] ) : false;
        // Do something with your data
        // NOTE: if you use your keys dynamically from the form that the Ajax post you need to sanitize those also
        if ( $view_header ) {
          $status = update_option( 'view_header', $view_header );
        }
        if ( $header_custom_logo ) {
          $status = update_option( 'header_custom_logo', $header_custom_logo );
        }
        // etc....
        // Or build an array and store all setting in one option field which is better for performance
        // DO NOT DO THIS --------------------------------------
        //      foreach ( $_POST as $key => $val ) {
        //        if ( $key != 'cfgeo_settings' ) {
        //          // $key should need to be santized if done this way
        //          update_option( $key, esc_attr( $val ) );
        //        }
        //      }
        // DO NOT DO THIS --------------------------------------
        if ( $status ) {
          echo 'true';
        } else {
          echo 'false';
        }
      } else {
        // If we fail return -1 since that is also what a failed nonce returns
        echo - 1;
      }
      //unset( $data['option_page'], $data['action'], $data['_wpnonce'], $data['_wp_http_referer'] );
      wp_die(); // this is required to terminate immediately and return a proper response


    // $data = $_POST;
    // unset( $data['option_page'], $data['action'], $data['_wpnonce'], $data['_wp_http_referer'] );
    // if ( update_option('main_options', $data ) ) {
    //   die(1);
    //   echo 'SUCCESS!';
    // }
    // else {
    //   die (0);
    // }
  }
  /**
   * RENDER SETTINGS TABS
   *
   * Creates and displays tab based GUI on plugin options page.
   *
   * @since     3.0.0
   * @version   1.1.0       Escape and translation added.
   * @return    mixed       The settings field
  */
   public function udtbp_render_tabs() {
     $current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'header';
     echo '<ul id="udt_panel_mainmenu" role="tablist">';
     foreach ( $this->plugin_settings_tabs as $tab_key => $tab_caption ) {
      $active = $current_tab == $tab_key ? 'nav-tab-active' : '';
      echo '<li class="ui-state-default' . esc_attr_e( $active ) . '" role="tab"><a class="ui-tabs-anchor" href="?page=' . esc_html( $this->udtbp ) . '&tab=' . esc_html( $tab_key ) . '">' . esc_html( $tab_caption ) . '</a></li>';
     }
     echo '</ul>';
   } // end udtbp_render_tabs()
  /**
   * PLUGIN SETTINGS LINK
   *
   * Add Support AND Settings Link on plugin page
   *
   * @since     3.0.0
   * @version   1.0.1     Removed Support Link because of pretty plugins bug.
   * @param     array     $actions    Array that merges settings and support links on plugin options page.
   * @return    mixed       The settings field
   * @link   https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
  */
  public function add_settings_link( $actions, $plugin_file ) {
    static $plugin;
        $settings = array( 'settings' => '<a class="'.esc_attr( $this->udtbp ).'-settings-link" href="options-general.php?page='.esc_html( $this->udtbp ).'">' . __( 'Settings' ) . '</a>' );
       // $site_link = array( 'support' => '<a title="Submit Help Request" target="_blank" href="//www.udel.edu/it/help/request/index.html">' . __( 'Support', $this->udtbp ) . '</a>' );
        $actions = array_merge( $settings, $actions );
       // $actions = array_merge( $site_link, $actions );
      return $actions;
   } // end add_settings_link()
  /**
  * ADMIN SETTINGS CALLBACK FUNCTION
  * Callback function for the admin settings page.
  *
  * @since    3.0.0
  * @version  1.0.1  Updated constant from UDTBP_DIR
  */
   public function display_plugin_admin_page(){
    include_once ( UDTBP_ADMIN_VIEWS_URL . 'udtbp-admin-display.php' );
   } // end display_plugin_admin_page()
} // end class udtbp_Admin
endif;
